package com.gskdroidev.memento.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

/**
 * Created by Saravanakumar on 02-06-2021.
 */
class CustomSpinnerAdapter(context: Context?, resouceId: Int, list: Array<String?>?) :
    ArrayAdapter<String?>(
        context!!, resouceId, list!!
    ) {
    override fun getDropDownView(
        position: Int, convertView: View?,
        parent: ViewGroup
    ): View {
        var view: View? = null
        if (position == 0) {
            val textView = TextView(context)
            textView.height = 0
            view = textView
        } else {
            view = super.getDropDownView(position, null, parent)
        }
        return view!!
    }
}