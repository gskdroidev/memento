package com.gskdroidev.memento.adapter

/**
 * Created by Saravanakumar on 02-06-2021.
 */
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gskdroidev.memento.R
import com.gskdroidev.memento.model.Task


class EventAdapter : RecyclerView.Adapter<EventAdapter.EventViewHolder>() {

    private var taskList: List<Task>? = null

    class EventViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val taskNameTv: TextView = itemView.findViewById(R.id.name)
        private val taskDateTv: TextView = itemView.findViewById(R.id.date)
        private val taskTypeTv: TextView = itemView.findViewById(R.id.type)

        fun bind(task: Task) {
            taskNameTv.text = task.name
            taskDateTv.text = task.date
            taskTypeTv.text = task.type
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.event_list_item, parent, false)
        return EventViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        taskList?.get(holder.adapterPosition)?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int {
        return taskList?.size ?: 0
    }


    fun updateEventList(taskList: List<Task>) {
        this.taskList = taskList
        notifyDataSetChanged()
    }
}
