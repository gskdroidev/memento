package com.gskdroidev.memento.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Task_Type")
data class TaskType(
    @ColumnInfo(name = "name") val name: String,
) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "s_no")
    var sNo: Int = 0

    /* @TypeConverters(DataTypeConverter::class)
     var extendedDB: ExtendedDB? = null*/

}

