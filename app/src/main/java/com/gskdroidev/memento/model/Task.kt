package com.gskdroidev.memento.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Task")
data class Task(
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "type") val type: String = "Default"
) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "s_no")
    var sNo: Int = 0


}

