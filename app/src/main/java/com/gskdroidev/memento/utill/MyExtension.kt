package com.gskdroidev.memento.utill

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.gskdroidev.memento.BaseActivity


fun Context.showToast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, text, duration).show()
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun inflate(context: Context, resource: Int, root: ViewGroup): View{
    return LayoutInflater.from(context).inflate(resource, root, false)
}

fun getVisibleFragment(context: BaseActivity): Fragment? {
    val fragments: List<Fragment> = context.supportFragmentManager.fragments
    for (fragment in fragments) {
        if (fragment.isVisible) return fragment
    }
    return null
}





