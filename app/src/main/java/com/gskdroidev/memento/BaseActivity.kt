package com.gskdroidev.memento

import android.os.Handler
import android.os.Looper
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.gskdroidev.memento.utill.showToast


abstract class BaseActivity : AppCompatActivity() {

    private var isBackPressedTwise = false

    fun setToolBar(title: String) {
        val toolbar = findViewById<Toolbar>(R.id.tool_bar)
        val toolBarTitle = toolbar.findViewById<TextView>(R.id.tool_bar_title)
        toolBarTitle.text = title
    }

    override fun onBackPressed() {
        if (isBackPressedTwise) {
            finish()
            return
        }

        this.isBackPressedTwise = true
        showToast("Please click BACK again to exit")
        Handler(Looper.getMainLooper()).postDelayed(Runnable { isBackPressedTwise = false }, 2000)
    }

}