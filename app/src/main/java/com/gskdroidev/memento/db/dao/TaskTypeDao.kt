package com.gskdroidev.memento.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gskdroidev.memento.model.Task
import com.gskdroidev.memento.model.TaskType

@Dao
interface TaskTypeDao {

    @Query("Select name from Task_Type order by name ASC")
    fun getAllTaskTypes() : LiveData<List<String>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(task: TaskType)

    @Query("Delete from Task_Type")
    fun deleteAll()
}