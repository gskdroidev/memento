package com.gskdroidev.memento.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.gskdroidev.memento.db.dao.TaskDao
import com.gskdroidev.memento.db.dao.TaskTypeDao
import com.gskdroidev.memento.model.Task
import com.gskdroidev.memento.model.TaskType

@Database(entities = [Task::class, TaskType::class], version = 1, exportSchema = true)
abstract class MementoDataBase : RoomDatabase() {

    abstract fun taskDao(): TaskDao
    abstract fun taskTypeDao(): TaskTypeDao

    companion object {
        private const val dataBaseName = "Memento_DB"
        private var INSTANCE: MementoDataBase? = null

        fun getDataBase(context: Context): MementoDataBase {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        MementoDataBase::class.java,
                        dataBaseName
                    ).fallbackToDestructiveMigration().build()
                }
            }

            return INSTANCE!!
        }

    }
}