package com.gskdroidev.memento.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gskdroidev.memento.model.Task

@Dao
interface TaskDao {

    @Query("Select * from Task order by s_no ASC")
    fun getAllTasks() : LiveData<List<Task>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(task: Task)

    @Query("Delete from Task")
    fun deleteAll()
}