package com.gskdroidev.memento

import android.app.Application

/**
 * Created by Saravanakumar on 26-03-2021.
 */
class MementoApplication : Application() {

    companion object {
        lateinit var application: MementoApplication

        @JvmStatic
        public fun getMementoApplication(): MementoApplication {
            return application;
        }
    }


    override fun onCreate() {
        super.onCreate()
        application = this
    }



}