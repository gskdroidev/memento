package com.gskdroidev.memento.dashboard

/**
 * Created by Saravanakumar on 02-06-2021.
 */
interface TaskListener {
    fun openAddEventScreen()
    fun newEventAdded()
    fun setToolBarTitle(title: String)
}