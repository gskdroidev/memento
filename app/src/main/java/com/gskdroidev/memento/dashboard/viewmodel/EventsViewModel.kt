package com.gskdroidev.memento.dashboard.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.gskdroidev.memento.model.Task
import com.gskdroidev.memento.dashboard.repository.EventsRepository
import com.gskdroidev.memento.db.MementoDataBase
import com.gskdroidev.memento.model.TaskType
import kotlinx.coroutines.launch

class EventsViewModel(application: Application) : AndroidViewModel(application) {

    private val eventsRepository: EventsRepository
    val allTasks: LiveData<List<Task>>
    val allTaskTypes: LiveData<List<String>>

    init {
        val taskDao = MementoDataBase.getDataBase(application).taskDao()
        val taskTypeDao = MementoDataBase.getDataBase(application).taskTypeDao()
        allTasks = taskDao.getAllTasks()
        allTaskTypes = taskTypeDao.getAllTaskTypes()
        eventsRepository = EventsRepository(taskDao)
    }

    fun insert(task: Task) = viewModelScope.launch {
        eventsRepository.insert(task)
    }
}