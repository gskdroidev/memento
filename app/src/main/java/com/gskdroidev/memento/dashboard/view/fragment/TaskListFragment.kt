package com.gskdroidev.memento.dashboard.view.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.gskdroidev.memento.MementoApplication
import com.gskdroidev.memento.R
import com.gskdroidev.memento.adapter.EventAdapter
import com.gskdroidev.memento.dashboard.TaskListener
import com.gskdroidev.memento.dashboard.viewmodel.EventsViewModel
import com.gskdroidev.memento.model.Task
import com.gskdroidev.memento.utill.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_task_list.*

class TaskListFragment : Fragment() {

    companion object {
        val TAG: String = TaskListFragment::class.java.simpleName
    }

    private lateinit var eventsViewModel: EventsViewModel
    private lateinit var mContext: Context
    private var listener: TaskListener? = null
    private var eventAdapter = EventAdapter()


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        listener = if (mContext is TaskListener) mContext as TaskListener else null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        eventsViewModel =
            ViewModelProvider(this, ViewModelFactory(MementoApplication.getMementoApplication()))
                .get(EventsViewModel::class.java)

        eventsViewModel.allTasks.observe(this,
            {
                setAdapter(it)
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_task_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener?.setToolBarTitle(getString(R.string.app_name))
        eventListRv.layoutManager = LinearLayoutManager(mContext)
        eventListRv.adapter = eventAdapter
        addEventFab.setOnClickListener {
            listener?.openAddEventScreen()
        }
    }

    private fun setAdapter(list: List<Task>) {
        eventAdapter.updateEventList(list)
    }

    override fun onDetach() {
        super.onDetach()
        Log.d(TAG, " onDetach")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, " onDestroy")
    }



}