package com.gskdroidev.memento.dashboard.view.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.gskdroidev.memento.BaseActivity
import com.gskdroidev.memento.R
import com.gskdroidev.memento.dashboard.TaskListener
import com.gskdroidev.memento.dashboard.view.fragment.AddTaskFragment
import com.gskdroidev.memento.dashboard.view.fragment.TaskListFragment
import com.gskdroidev.memento.utill.getVisibleFragment


class DashBoardActivity : BaseActivity(), TaskListener {

    private val eventListFragment = TaskListFragment()
    private val addEventFragment = AddTaskFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setCurrentFragment(eventListFragment)
    }


    private fun setCurrentFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (!fragment.isAdded) {
            fragmentTransaction.add(
                R.id.fragment_container,
                fragment,
                fragment::class.java.simpleName
            ).addToBackStack(fragment::class.java.simpleName).commit()
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments.size > 1) {
            popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    private fun popBackStack() {
        supportFragmentManager.popBackStack()
    }

    override fun openAddEventScreen() {
        setCurrentFragment(addEventFragment)
    }

    override fun newEventAdded() {
        onBackPressed()
    }

    override fun setToolBarTitle(title: String) {
        setToolBar(title)
    }


}
