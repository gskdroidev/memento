package com.gskdroidev.memento.dashboard.view.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.gskdroidev.memento.MementoApplication
import com.gskdroidev.memento.R
import com.gskdroidev.memento.adapter.CustomSpinnerAdapter
import com.gskdroidev.memento.dashboard.TaskListener
import com.gskdroidev.memento.dashboard.viewmodel.EventsViewModel
import com.gskdroidev.memento.model.Task
import com.gskdroidev.memento.utill.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_add_task.*


class AddTaskFragment : Fragment() {

    companion object {
        val TAG = AddTaskFragment::class.java.simpleName
    }

    private lateinit var mContext: Context
    private lateinit var eventsViewModel: EventsViewModel

    private var listener: TaskListener? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        listener = if (mContext is TaskListener) mContext as TaskListener else null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        eventsViewModel =
            ViewModelProvider(this, ViewModelFactory(MementoApplication.getMementoApplication()))
                .get(EventsViewModel::class.java)


        eventsViewModel.allTaskTypes.observe(this, {
            if (it.isNotEmpty())
                setSpinner(it)
            else{
                val list = listOf("Default")
                setSpinner(list)
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener?.setToolBarTitle("Add Task")
        addTaskButton.setOnClickListener {
            val taskName = taskNameEditText.text.toString()
            val taskDate = taskDateEditText.text.toString()
            val taskType = taskTypeSpinner.selectedItem.toString()
            eventsViewModel.insert(Task(taskName, taskDate, taskType))
            listener?.newEventAdded()
        }
    }

    private fun setSpinner(data: List<String>) {
        val spinnerAdapter = CustomSpinnerAdapter(
            mContext,
            android.R.layout.simple_spinner_item,
            data.toTypedArray()
        )
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        taskTypeSpinner.adapter = spinnerAdapter

    }


    override fun onDetach() {
        super.onDetach()
        Log.d(TAG, " onDetach")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, " onDestroy")
    }


}