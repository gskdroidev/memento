package com.gskdroidev.memento.dashboard.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.gskdroidev.memento.db.dao.TaskDao
import com.gskdroidev.memento.model.Task

class EventsRepository(private val eventDao: TaskDao) {

    var allTasks : LiveData<List<Task>> = eventDao.getAllTasks()

    @WorkerThread
    suspend fun insert(task: Task){
        eventDao.insert(task)
    }
}